#Ensure you edit the variables.tf file and run terraform from the directory containing both files

resource "azurerm_public_ip" "bastion" {
  name                = "bastionip"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group}"
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_subnet" "bastion" {
    name = "AzureBastionSubnet"
    address_prefix = "${var.bastion_subnet_address_space}"
    virtual_network_name = "${var.vnet_name}"
    resource_group_name = "${var.resource_group}"
}

resource "azurerm_bastion_host" "main" {
    name = "mybastion"
    resource_group_name = "${var.resource_group}"
    location = "${var.location}"
    depends_on = [azurerm_subnet.bastion]
        ip_configuration {
        name                 = "bastionipconf"
        subnet_id            = "${azurerm_subnet.bastion.id}"
        public_ip_address_id = "${azurerm_public_ip.bastion.id}"
    }
}
#Fill in desired location here, you must use a region that bastion is supported in such as West Europe
variable "location" {
    default = "westeurope"
}
#Fill in your current VNET name here
variable "vnet_name" {
    default = "prod_vnet"
}
#Fill in your current VNET address space here using CIDR notation
variable "vnet_address_space" {
    default = ["10.0.0.0/16"]
}
#Fill in your VNET's resource group here
variable "resource_group" {
    default = "prod-resources"
}
#Fill in your Bastion Subnet address space here, must be at least a /27 subnet
variable "bastion_subnet_address_space" {
    default = "10.0.10.192/27"
}

